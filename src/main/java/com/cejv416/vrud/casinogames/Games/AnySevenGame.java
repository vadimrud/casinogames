package com.cejv416.vrud.casinogames.Games;

import com.cejv569.vrud.casinogames.Calculations.AccountCalculations;
import com.cejv569.vrud.casinogames.Calculations.DieCalculations;
import com.cejv569.vrud.casinogames.beans.AccountBean;
import com.cejv569.vrud.casinogames.beans.DiceBean;


/**
 *
 * @author v_rudma
 */
public class AnySevenGame {    

    AccountBean accountBean;
    DiceBean diceBean;    
    AccountCalculations accountCalculations;
    DieCalculations dieCalculations;
    
    public AnySevenGame(AccountBean accountBean, DiceBean diceBean){
        super();
        this.accountBean = accountBean;
        this.diceBean = diceBean;
        accountCalculations = new AccountCalculations(this.accountBean);
        dieCalculations = new DieCalculations(this.diceBean);
    }
    
    /**
     * this method is called to play one round of Any 7
     * @param betsize 
     * @return 0 if lose
     *         4 if win
     */
    public int playAnySeven(double betsize){
        int gameOutcome = 0;
        accountCalculations.makeBet(betsize);
        dieCalculations.rollBothDie();
        if(dieCalculations.diceTotal() == 7){
            accountCalculations.winBetMultiplier(5);
            gameOutcome = 4;
        }
        return gameOutcome;
    }
    
    /**
     * This method is called to run the game.
     * @param betsize
     * @return -1 when insufficient funds
     *         0 when you lose
     *         1 when you win
     */
    public int mainAnySevenGame(double betsize){
        int gameOutcome;
        if(accountCalculations.isEnoughMoney(betsize)){
            gameOutcome = playAnySeven(betsize);
        }
        else{
            gameOutcome = -1;
        }
        return gameOutcome;
    }
    
    
    
    
}
