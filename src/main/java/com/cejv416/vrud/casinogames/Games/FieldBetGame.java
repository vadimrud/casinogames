package com.cejv416.vrud.casinogames.Games;

import com.cejv569.vrud.casinogames.Calculations.AccountCalculations;
import com.cejv569.vrud.casinogames.Calculations.DieCalculations;
import com.cejv569.vrud.casinogames.beans.AccountBean;
import com.cejv569.vrud.casinogames.beans.DiceBean;

/**
 *
 * @author v_rudma
 */
public class FieldBetGame {
    
    AccountBean accountBean;
    DiceBean diceBean;
    AccountCalculations accountCalculations;
    DieCalculations dieCalculations;
    
    public FieldBetGame(AccountBean accountBean, DiceBean diceBean){
        super();
        this.accountBean = accountBean;
        this.diceBean = diceBean;
        accountCalculations = new AccountCalculations(this.accountBean);
        dieCalculations = new DieCalculations(this.diceBean);
    }
    
    /**
     * this method is called to play one round of Field Bet
     * @param betsize 
     * @return 0 if lose
     *         1 if win (roll 3,4,9,10,11)
     *         2 if win (roll 2)
     *         3 if win (roll 12)
     */
    public int playFieldBetGame(double betsize){
        int gameOutcome = 0;
        accountCalculations.makeBet(betsize);
        dieCalculations.rollBothDie();
        int diceTotal = dieCalculations.diceTotal();
        switch(diceTotal){
            
            case 2:                                   accountCalculations.winBetMultiplier(3);
                                                      gameOutcome = 2;
                                                      break;
                                                      
            case 3: case 4: case 9: case 10: case 11: accountCalculations.winBetMultiplier(2);
                                                      gameOutcome = 1;
                                                      break;
                                                      
            case 5: case 6: case 7: case 8:           break;
            
            case 12:                                  accountCalculations.winBetMultiplier(4);
                                                      gameOutcome = 3;
                                                      break;
            
        }
        return gameOutcome;
    }
    
    /**
     * This method is called to run the game.
     * @param betsize
     * @return 0 when insufficient funds
     *         1 when sufficient funds 
     */
    public int mainFieldBetGame(double betsize){
        int gameOutcome;
        if(accountCalculations.isEnoughMoney(betsize)){
            gameOutcome = playFieldBetGame(betsize);
        }
        else{
            gameOutcome = 0;
        }
        return gameOutcome;       
    }
    
}
