package com.cejv416.vrud.casinogames.Games;

import com.cejv569.vrud.casinogames.Calculations.AccountCalculations;
import com.cejv569.vrud.casinogames.Calculations.DieCalculations;
import com.cejv569.vrud.casinogames.beans.AccountBean;
import com.cejv569.vrud.casinogames.beans.DiceBean;

/**
 *
 * @author v_rudma
 */
public class PassLineGame {

    AccountBean accountBean;
    DiceBean diceBean;    
    AccountCalculations accountCalculations;
    DieCalculations dieCalculations;

    public PassLineGame(AccountBean accountBean, DiceBean diceBean) {
        super();
        this.accountBean = accountBean;
        this.diceBean = diceBean;
        accountCalculations = new AccountCalculations(this.accountBean);
        dieCalculations = new DieCalculations(this.diceBean);        
    }

    /**
     * This method is called to play the first roll which also withdraws money
     * from the user
     *
     * @param betsize
     * @return the game outcome (dice total), 1 if win and 0 if loss
     */
    public int firstRoll(double betsize) {
        accountCalculations.makeBet(betsize);
        dieCalculations.rollBothDie();
        int gameOutcome = dieCalculations.diceTotal();
        if (dieCalculations.diceTotal() == 7 || dieCalculations.diceTotal() == 11) {
            accountCalculations.winBetMultiplier(2);
            gameOutcome = 1;
        }
        if (dieCalculations.diceTotal() == 2 || dieCalculations.diceTotal() == 3 || dieCalculations.diceTotal() == 12 ) {
            gameOutcome =0;
        }
        return gameOutcome;
    }
    
    /**
     * this method will be called to play the first roll
     * @param betsize
     * @return -1 if insufficient funds, 0 if loss, 1 if win, and dice total otherwise
     */
    public int playFirstRoll(double betsize){
        int gameOutcome;
        if(accountCalculations.isEnoughMoney(betsize)){
            gameOutcome = firstRoll(betsize);
        }
        else{
            gameOutcome = -1;
        }
        return gameOutcome;
    }
    
    /**
     * This method is called to play subsequent rolls
     * and add money to the balance if there's a win
     * @param point
     * @return 0 if loss, 1 if win, point if game must continue
     */
    public int subsequentRoll(int point){
        int gameOutcome;
        dieCalculations.rollBothDie();
        if(dieCalculations.diceTotal() == point){            
            accountCalculations.winBetMultiplier(2);
            gameOutcome = 1;
        } 
        else if(dieCalculations.diceTotal() == 7){
            gameOutcome = 0;
        }
        else 
            gameOutcome = point; 
        
        return gameOutcome; 
        
    }
    
    /**
     * This method is not used! DOESNT ALLOW USER TO PRESS BTN AFTER EVERY ROUND
     * 
     * This method is called to run the game.
     * @param betsize
     * @return 0 when insufficient funds
     *         1 when sufficient funds
     */
    public int mainPassLineGame(double betsize){
        int isPlay;
        if(accountCalculations.isEnoughMoney(betsize)){
            int firstRoll = firstRoll(betsize);
            int isContinueGame = subsequentRoll(firstRoll);
            
            while(isContinueGame == 0){
                isContinueGame = subsequentRoll(firstRoll);
            }
            
            isPlay = 1;
        }
        else{
            isPlay = 0;
        }
        return isPlay;        
    }

}
