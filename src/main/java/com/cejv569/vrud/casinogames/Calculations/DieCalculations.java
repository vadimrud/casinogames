package com.cejv569.vrud.casinogames.Calculations;

import com.cejv569.vrud.casinogames.beans.DiceBean;

/**
 *
 * @author v_rudma
 */
public class DieCalculations {
    
    DiceBean diceBean;
    
    /**
     * this class will handle all calculations regarding the dice themselves
     */
    public DieCalculations(DiceBean diceBean){
        super();
        this.diceBean = diceBean;
    }
    
    /**
     * rng for first die
     */
    public void rollDieOne(){
        diceBean.setDieOne((int)(Math.random() * 6 + 1));
    }
    
    /**
     * rng for second die
     */
    public void rollDieTwo(){
        diceBean.setDieTwo((int)(Math.random() * 6 + 1));
    }
    
    /**
     * this method is called to roll both dies (rng both dice)
     */
    public void rollBothDie(){
        rollDieOne();
        rollDieTwo();
    }
    
    /**
     * 
     * @return the sum of both dice
     */
    public int diceTotal(){
        return (diceBean.getDieOne() + diceBean.getDieTwo());
    }
    
    
}
