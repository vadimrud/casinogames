package com.cejv569.vrud.casinogames.Calculations;

import com.cejv569.vrud.casinogames.beans.AccountBean;

/**
 *
 * @author v_rudma
 */
public class AccountCalculations {

    AccountBean accountBean;

    /**
     * this class will handle all calculations regarding both dice
     * @param accountBean
     */
    public AccountCalculations(AccountBean accountBean) {
        super();
        this.accountBean = accountBean;
    }

    //Big decimal later?????
    /**
     * This method will be called when user wants to add $$$ to total funds
     *
     * @param deposit
     */
    public void addFunds(double deposit) {
        accountBean.setTotalFunds(accountBean.getTotalFunds() + deposit);
    }

    /**
     * this method will be called when the user places a bet to subtract the $$
     * from total funds and set the bet amount
     *
     * @param betsize
     */
    public void makeBet(double betsize) {
        accountBean.setTotalFunds(accountBean.getTotalFunds() - betsize);
        accountBean.setBetAmount(betsize);
    }

    /**
     * This method will be called whenever the user wins a bet
     *
     * @param multiplier
     */
    public void winBetMultiplier(double multiplier) {
        addFunds(accountBean.getBetAmount() * multiplier);
    }

    /**
     * this method will be called to check if user has enough $$$ to bet
     *
     * @param betsize
     * @return
     */
    public boolean isEnoughMoney(double betsize) {
        boolean isAllow;
        if (betsize > accountBean.getTotalFunds()) {
            isAllow = false;
        } else {
            isAllow = true;
        }
        return isAllow;
    }

}
