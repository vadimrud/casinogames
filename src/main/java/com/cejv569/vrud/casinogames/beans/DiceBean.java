package com.cejv569.vrud.casinogames.beans;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

public class DiceBean {
    
    private IntegerProperty dieOne;
    private IntegerProperty dieTwo;
    
    public DiceBean(){
        super();
        this.dieOne = new SimpleIntegerProperty(0);
        this.dieTwo = new SimpleIntegerProperty(0);
    }

    public int getDieOne() {
	return dieOne.get();
    }

    public void setDieOne(final int dieOne) {
	this.dieOne.set(dieOne);
    }

    public IntegerProperty idDieOne() {
	return dieOne;
    }

    public int getDieTwo() {
	return dieTwo.get();
    }

    public void setDieTwo(final int dieTwo) {
	this.dieTwo.set(dieTwo);
    }

    public IntegerProperty idDieTwo() {
	return dieTwo;
    }
    
    @Override
    public String toString(){
        
        return "Die one: " + getDieOne()
              +"\nDie two: " + getDieTwo();
        
    }
    
    
    
    
    
}
