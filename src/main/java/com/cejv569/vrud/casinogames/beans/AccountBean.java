package com.cejv569.vrud.casinogames.beans;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author v_rudma
 */
public class AccountBean {
    
    private StringProperty name;
    private StringProperty email;
    private DoubleProperty totalFunds;
    private DoubleProperty betAmount;
    
    public AccountBean(){
        super();
        this.name = new SimpleStringProperty("");
        this.email = new SimpleStringProperty("");
        this.totalFunds = new SimpleDoubleProperty(0.0);
        this.betAmount = new SimpleDoubleProperty(0.0);
    }

    public String getName() {
	return name.get();
    }

    public void setName(final String name) {
	this.name.set(name);
    }

    public StringProperty nameProperty() {
	return name;
    }

    public String getEmail() {
	return email.get();
    }

    public void setEmail(final String email) {
	this.name.set(email);
    }

    public StringProperty emailProperty() {
	return email;
    }

    public double getTotalFunds() {
	return totalFunds.get();
    }

    public void setTotalFunds(final double totalFunds) {
	this.totalFunds.set(totalFunds);
    }

    public DoubleProperty totalFundsProperty() {
	return totalFunds;
    }

    public double getBetAmount() {
	return betAmount.get();
    }

    public void setBetAmount(final double betAmount) {
	this.betAmount.set(betAmount);
    }

    public DoubleProperty betAmountProperty() {
	return betAmount;
    }
    
    @Override
    public String toString(){
        return "Account name: " + getName()
              +"/nEmail address: " + getEmail()
              +"/nTotal funds: " + getTotalFunds();
    }
    
    
    
}
