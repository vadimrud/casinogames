/**
 * Sample Skeleton for 'CasinoGames.fxml' Controller Class
 */
package com.cejv569.vrud.casinogames.controller;

import com.cejv416.vrud.casinogames.Games.AnySevenGame;
import com.cejv416.vrud.casinogames.Games.FieldBetGame;
import com.cejv416.vrud.casinogames.Games.PassLineGame;
import com.cejv569.vrud.casinogames.Calculations.AccountCalculations;
import com.cejv569.vrud.casinogames.Calculations.DieCalculations;
import com.cejv569.vrud.casinogames.beans.AccountBean;
import com.cejv569.vrud.casinogames.beans.DiceBean;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.util.converter.NumberStringConverter;

public class CasinoGamesController {

    private AccountBean accountBean;
    private DiceBean diceBean;
    private AnySevenGame anySevenGame;
    private FieldBetGame fieldBetGame;
    private PassLineGame passLineGame;
    private DieCalculations dieCalculations;
    private int gameChoice;
    private int gameOutcome;
    private int point = 0;

    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="nameTextField"
    private TextField nameTextField; // Value injected by FXMLLoader

    @FXML // fx:id="emailTextField"
    private TextField emailTextField; // Value injected by FXMLLoader

    @FXML // fx:id="balanceTextField"
    private TextField balanceTextField; // Value injected by FXMLLoader

    @FXML // fx:id="any7RadioBtn"
    private RadioButton any7RadioBtn; // Value injected by FXMLLoader

    @FXML // fx:id="fieldBetRadioBtn"
    private RadioButton fieldBetRadioBtn; // Value injected by FXMLLoader

    @FXML // fx:id="passLineRadioBtn"
    private RadioButton passLineRadioBtn; // Value injected by FXMLLoader

    @FXML // fx:id="saveBtn"
    private Button saveBtn; // Value injected by FXMLLoader

    @FXML // fx:id="clearBtn"
    private Button clearBtn; // Value injected by FXMLLoader

    @FXML // fx:id="rollDieBtn"
    private Button rollDieBtn; // Value injected by FXMLLoader

    @FXML // fx:id="howToPlayTextArea"
    private TextArea howToPlayTextArea; // Value injected by FXMLLoader

    @FXML // fx:id="outcomeTextArea"
    private TextArea outcomeTextArea; // Value injected by FXMLLoader

    @FXML // fx:id="exitBtn"
    private Button exitBtn; // Value injected by FXMLLoader

    @FXML // fx:id="betSizeTextField"
    private TextField betSizeTextField; // Value injected by FXMLLoader

    public CasinoGamesController() {
        super();
        accountBean = new AccountBean();
        diceBean = new DiceBean();
        anySevenGame = new AnySevenGame(accountBean, diceBean);
        fieldBetGame = new FieldBetGame(accountBean, diceBean);
        passLineGame = new PassLineGame(accountBean, diceBean);
        dieCalculations = new DieCalculations(diceBean);
    }

    @FXML
    void onAny7(ActionEvent event) {
        any7RadioBtn.setSelected(true);
        passLineRadioBtn.setSelected(false);
        fieldBetRadioBtn.setSelected(false);
        gameChoice = 1;
        howToPlayTextArea.setText("Roll the die, if you roll a 7 (seven) you win 4 (four) times your bet amount!"
                + "\nif you roll another amount, you lose your bet amount.");
    }

    @FXML
    void onClear(ActionEvent event) {
        nameTextField.setText("");
        emailTextField.setText("");
        balanceTextField.setText("");
        betSizeTextField.setText("");
        nameTextField.setEditable(true);
        emailTextField.setEditable(true);
        balanceTextField.setEditable(true);
    }

    @FXML
    void onExit(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void onFieldBet(ActionEvent event) {
        any7RadioBtn.setSelected(false);
        passLineRadioBtn.setSelected(false);
        fieldBetRadioBtn.setSelected(true);
        gameChoice = 2;
        howToPlayTextArea.setText("Roll the die, if your roll is 3, 4, 9, 10 or 11, you win your bet amount!"
                + "\nIf your roll is 2, you win 2 (two) times your bet amount!"
                + "\nIf your roll is 12, you win 3 (three) times your bet amount!"
                + "\nOn any other amount (5, 6, 7 or 8), you lose your bet amount.");
    }

    @FXML
    void onPassLine(ActionEvent event) {
        any7RadioBtn.setSelected(false);
        passLineRadioBtn.setSelected(true);
        fieldBetRadioBtn.setSelected(false);
        gameChoice = 3;
        howToPlayTextArea.setText("Roll the die, if your roll is 7 or 11, you win your bet amount!"
                + "\nif you roll 2, 3 or 12, you lose your bet amount."
                + "\nOn any other roll, your roll becomes the point."
                + "\nContinue to roll until you roll either"
                + "\n7: You lose your bet amount" 
                + "\nthe point: You win your bet amount");
    }

    @FXML
    void onRollDie(ActionEvent event) {

        switch (gameChoice) {
            case 1:
                gameOutcome = anySevenGame.mainAnySevenGame(accountBean.getBetAmount());
                displayMessage(dieCalculations.diceTotal());
                //remember to check for win/loss
                break;
            case 2:
                gameOutcome = fieldBetGame.mainFieldBetGame(accountBean.getBetAmount());
                displayMessage(dieCalculations.diceTotal());
                //add check for win loss
                break;
            case 3:
                if (point == 0) {
                    gameOutcome = passLineGame.playFirstRoll(accountBean.getBetAmount());
                    displayMessageFirstRoll(dieCalculations.diceTotal(), accountBean.getBetAmount());
                } else if (point > 1) {
                    gameOutcome = passLineGame.subsequentRoll(point);
                    displayMessageSubsequentRolls(dieCalculations.diceTotal(), accountBean.getBetAmount());
                    break;

                }

        }
    }

    @FXML
    void onSave(ActionEvent event) {
        nameTextField.setEditable(false);
        emailTextField.setEditable(false);
        balanceTextField.setEditable(false);
        rollDieBtn.setVisible(true);
        outcomeTextArea.setText("");
    }

    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        Bindings.bindBidirectional(nameTextField.textProperty(), accountBean.nameProperty());
        Bindings.bindBidirectional(emailTextField.textProperty(), accountBean.emailProperty());
        Bindings.bindBidirectional(balanceTextField.textProperty(), accountBean.totalFundsProperty(), new NumberStringConverter());
        Bindings.bindBidirectional(betSizeTextField.textProperty(), accountBean.betAmountProperty(), new NumberStringConverter());
        outcomeTextArea.setText("Please \"save\" your account information!");
        rollDieBtn.setVisible(false);
        outcomeTextArea.setEditable(false);
        howToPlayTextArea.setEditable(false);
        balanceTextField.setText("");
        betSizeTextField.setText("");
    }
    
    /**
     * this method is called to show the outcome message for any 7 and field bet
     * @param dieTotal 
     */
    public void displayMessage(int dieTotal){
	
	if (gameOutcome == -1) {
		outcomeTextArea.setText("Insufficient funds!");
	} else if (gameOutcome == 0) {
		outcomeTextArea.setText("You rolled a " + dieTotal + "\nSorry, you lose " + accountBean.getBetAmount() + "$");
	} else if (gameOutcome > 0){
		outcomeTextArea.setText("You rolled a " + dieTotal + "\nCongradulations!! You win " + accountBean.getBetAmount() * gameOutcome + "$");
	}

}
    /**
     * this method is called to show to outcome message for the first die roll
     * for the pass line game
     * @param dieTotal
     * @param betAmount 
     */
    public void displayMessageFirstRoll(int dieTotal, double betAmount) {

        switch (gameOutcome) {
            case -1:
                outcomeTextArea.setText("Insufficient funds!");
                break;
            case 0:
                outcomeTextArea.setText("You rolled a " + dieTotal + "\nSorry, you lose " + accountBean.getBetAmount() + "$");
                break;
            case 1:
                outcomeTextArea.setText("You rolled a " + dieTotal + "\nCongradulations!! You win " + accountBean.getBetAmount() + "$");
                break;
            default:
                point = gameOutcome;
                outcomeTextArea.setText("You rolled a " + dieCalculations.diceTotal()
                        + "\nThat is now the point, please roll again."
                        + "\n" + gameOutcome);
                break;
        }

    }
    
    /**
     * this method is called to show to outcome message for subsequent rolls
     * for the pass line game
     * @param dieTotal
     * @param betAmount 
     */
    public void displayMessageSubsequentRolls(int dieTotal, double betAmount) {

        switch (gameOutcome) {
            case 0:
                outcomeTextArea.setText("You rolled a " + dieTotal + "\nSorry, you lose " + accountBean.getBetAmount() + "$");
                point = 0;
                break;
            case 1:
                outcomeTextArea.setText("You rolled a " + dieTotal + "\nCongradulations!! You win " + accountBean.getBetAmount() + "$");
                point = 0;
                break;
            default:
                outcomeTextArea.setText("You rolled a " + dieCalculations.diceTotal()
                        + "\n" + point + " is still the point, please roll again."
                        + "\n" + gameOutcome);
                break;
        }

    }

}
